﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class EmpleadoFactura
    {
        private int cod;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;

        public int Cod
        {
            get
            {
                return cod;
            }

            set
            {
                cod = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public EmpleadoFactura(int cod, DateTime fecha, double subtotal, double iva, double total)
        {
            this.Cod = cod;
            this.Fecha = fecha;
            this.Subtotal = subtotal;
            this.Iva = iva;
            this.Total = total;
        }
    }
}
